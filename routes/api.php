<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=> 'v1', 'as'=> 'v1.', 'namespace' => 'Api\\'], function(){
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::post('projects/add-user-to-project', 'ProjectsController@addUserToProject')
            ->name('projects.add-user-to-project');

        Route::get('users/get-count-users', 'UsersController@countUsers')
            ->name('users.get-count-users');

        Route::get('projects/get-count-projects', 'ProjectsController@countProjects')
            ->name('projects.get-count-projects');

        Route::get('researches/get-count-researches', 'ResearchesController@countResearches')
            ->name('researches.get-count-researches');

        Route::get('news/get-count-news', 'NewsController@countNews')
            ->name('news.get-count-news');

        Route::resource('users', 'UsersController', ['except' => ['create', 'edit']]);

        Route::resource('projects', 'ProjectsController');

        Route::resource('news', 'NewsController');

        Route::resource('usefulinks', 'UsefulLinksController');

        Route::post('projects/add-instituion-to-project', 'ProjectsController@addInstitutionToProject')
            ->name('projects.add-instituion-to-project');

        Route::resource('institutions', 'InstitutionsController');

        Route::post('researches/add-user-to-research', 'ResearchesController@addUserToResearch')
            ->name('researches.add-user-to-research');

        Route::post('researches/add-institution-to-research', 'ResearchesController@addInstitutionToResearch')
            ->name('researches.add-institution-to-research');

        Route::resource('researches', 'ResearchesController');

        Route::post('productions/add-user-to-production', 'ProductionsController@addUserToProduction')
            ->name('productions.add-user-to-production');

        Route::resource('productions', 'ProductionsController');

        Route::get('profiles', 'ProfilesController@index')
            ->name('profiles');
    });

    Route::post('/login', 'AuthController@authenticate');
});
