<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status_posted', 1);
            $table->string('link', 255);
            $table->longText('text');
            $table->string('reference', 255);
            $table->string('comments', 255);
            $table->string('url_cover');
            $table->string('status', 1);
            $table->integer('production_type_id')->unsigned();
            $table->foreign('production_type_id')->references('id')->on('productions_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productions');
    }
}
