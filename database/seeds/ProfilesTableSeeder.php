<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            [
                'name' => 'Admin'
            ],
            [
                'name' => 'Coordenador de Projeto'
            ],
            [
                'name' => 'Coordenador de Pesquisa'
            ],
            [
                'name' => 'Pesquisador'
            ]
        ]);
    }
}
