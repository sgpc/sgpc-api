<?php

use Illuminate\Database\Seeder;

class ProductionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productions_type')->insert([
            [
                'name' => 'Artigo'
            ],
            [
                'name' => 'Produção de Campo'
            ]
        ]);
    }
}
