<?php

namespace Sgpc\Providers;

use Illuminate\Support\ServiceProvider;
use Sgpc\Repositories\UserRepository;
use Sgpc\Repositories\UserRepositoryEloquent;
use Sgpc\Repositories\ProjectRepository;
use Sgpc\Repositories\ProjectRepositoryEloquent;
use Sgpc\Repositories\NewsRepository;
use Sgpc\Repositories\NewsRepositoryEloquent;
use Sgpc\Repositories\UsefulLinkRepository;
use Sgpc\Repositories\UsefulLinkRepositoryEloquent;
use Sgpc\Repositories\InstitutionRepository;
use Sgpc\Repositories\InstitutionRepositoryEloquent;
use Sgpc\Repositories\ResearchRepository;
use Sgpc\Repositories\ResearchRepositoryEloquent;
use Sgpc\Repositories\ProductionRepository;
use Sgpc\Repositories\ProductionRepositoryEloquent;

class SgpcRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
        $this->app->bind(ProjectRepository::class, ProjectRepositoryEloquent::class);
        $this->app->bind(NewsRepository::class, NewsRepositoryEloquent::class);
        $this->app->bind(UsefulLinkRepository::class, UsefulLinkRepositoryEloquent::class);
        $this->app->bind(InstitutionRepository::class, InstitutionRepositoryEloquent::class);
        $this->app->bind(ResearchRepository::class, ResearchRepositoryEloquent::class);
        $this->app->bind(ProductionRepository::class, ProductionRepositoryEloquent::class);
    }
}
