<?php

namespace Sgpc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Production extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'status_posted', 'link', 'text', 'reference', 'comments', 'url_cover', 'status', 'production_type_id'
    ];

}
