<?php

namespace Sgpc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Profile extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function relationsProject()
    {
        return $this->hasMany(ParticipantProject::class, 'profile_id');
    }

    public function relationsResearch()
    {
        return $this->hasMany(ParticipantResearch::class, 'profile_id');
    }

    public function users()
    {
        return $this->hasOne(User::class);
    }
}
