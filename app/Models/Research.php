<?php

namespace Sgpc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Research extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'title', 'summary', 'goals'
    ];

    public function relationsResearch()
    {
        return $this->hasMany(ParticipantResearch::class, 'research_id');
    }

    public function institutions()
    {
        return $this->belongsToMany(Institution::class);
    }

}
