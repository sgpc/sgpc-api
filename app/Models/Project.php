<?php

namespace Sgpc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Project extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'summary', 'justification', 'goals'
    ];

    public function relationsProject()
    {
        return $this->hasMany(ParticipantProject::class, 'project_id');
    }

    public function relationsResearch()
    {
        return $this->hasMany(ParticipantResearch::class, 'project_id');
    }

    public function institutions()
    {
        return $this->belongsToMany(Institution::class);
    }
}
