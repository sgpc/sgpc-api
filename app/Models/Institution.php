<?php

namespace Sgpc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Institution extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'name', 'initials'
    ];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function researches()
    {
        return $this->hasMany(Research::class);
    }
}
