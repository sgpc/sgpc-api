<?php

namespace Sgpc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ParticipantResearch extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $fillable = [
        'user_id', 'research_id', 'profile_id'
    ];

    function research() {
        return $this->belongsTo(Research::class, 'research_id');
    }

    function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    function profile() {
        return $this->belongsTo(Profile::class, 'profile_id');
    }

}
