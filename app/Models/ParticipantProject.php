<?php

namespace Sgpc\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ParticipantProject extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id', 'profile_id', 'user_id'
    ];

    function project() {
        return $this->belongsTo(Project::class, 'project_id');
    }

    function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    function profile() {
        return $this->belongsTo(Profile::class, 'profile_id');
    }
}
