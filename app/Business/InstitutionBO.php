<?php

namespace Sgpc\Business;

use Sgpc\Http\Requests\InstitutionRequest;
use Sgpc\Repositories\InstitutionRepository;

class InstitutionBO extends AbstractBO
{
    /**
     * @var InstitutionRepository
     */
    private $repository;

    /**
     * InstitutionBO constructor.
     * @param InstitutionRepository $repository
     */
    public function __construct(InstitutionRepository $repository)
    {
        $this->repository = $repository;
    }

    /*
	* Show all Institution.
	*/
    public function all()
    {
        return $this->repository->all();
    }

    /*
	 * Show a specific Institution.
	 *
	 * @param $id
	 */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Store a new Institution.
     *
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(array $attributes)
    {
        $this->beginTransaction();

        $newInstitution = $this->repository->create($attributes);

        $this->commit();

        return $newInstitution;
    }

    /*
	 * Update data from Institution.
	 *
	 * @param $request
	 * @param $id
	 */
    public function update(InstitutionRequest $request, $id)
    {
        $institution = $this->repository->find($id)->update($request->all());

        return $institution;
    }

    /*
	 * Delete a Institution.
	 *
	 * @param $id
	 */
    public function destroy($id)
    {
        $this->repository->find($id)->delete();
    }
}