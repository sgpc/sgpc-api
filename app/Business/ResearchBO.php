<?php

namespace Sgpc\Business;

use Sgpc\Http\Requests\ResearchRequest;
use Sgpc\Repositories\ResearchRepository;
use Sgpc\Models\Profile;
use Sgpc\Models\User;
use Sgpc\Models\ParticipantResearch;

class ResearchBO extends AbstractBO
{
    /**
     * @var ResearchRepository
     */
    private $repository;

    /**
     * UserBO constructor.
     * @param ResearchRepository $repository
     */
    public function __construct(ResearchRepository $repository)
    {
        $this->repository = $repository;
    }

    /*
	* Show all Researchs
	*/
    public function all()
    {
        return $this->repository->all();
    }

    /*
	 * Show a specific user.
	 *
	 * @param $id
	 */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Salva um novo usuário.
     *
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(array $attributes)
    {
        $this->beginTransaction();

        $newResearch = $this->repository->create($attributes);

        $this->commit();

        return $newResearch;
    }

    /*
	 * Update data from users.
	 *
	 * @param $request
	 * @param $id
	 */
    public function update(ResearchRequest $request, $id)
    {
        $Research = $this->repository->find($id)->update($request->all());

        return $Research;
    }

    /*
	 * Delete a Research.
	 *
	 * @param $id
	 */
    public function destroy($id)
    {
        $this->repository->find($id)->delete();
    }

    /*
    *
    */
    public function addUserToResearch($attributes)
    {
        $research = $this->repository->find($attributes->research_id);
        $profile = Profile::find($attributes->profile_id);
        $user = User::find($attributes->user_id);

        $participantResearch = new ParticipantResearch();
        $participantResearch->user()->associate($user);
        $participantResearch->research()->associate($research);
        $participantResearch->profile()->associate($profile);
        $participantResearch->save();
    }

    /*
    *
    */
    public function addInstitutionToResearch($attributes)
    {
        $research = $this->repository->find($attributes->research_id);
        $research->institutions()->attach($attributes->institution_id);
    }
}