<?php

namespace Sgpc\Business;

use Sgpc\Http\Requests\NewsRequest;
use Sgpc\Repositories\NewsRepository;

class NewsBO extends AbstractBO
{
    /**
     * @var NewsRepository
     */
    private $repository;

    /**
     * NewsBO constructor.
     * @param NewsRepository $repository
     */
    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
    }

    /*
	* Show all news.
	*/
    public function all()
    {
        return $this->repository->all();
    }

    /*
	 * Show a specific news.
	 *
	 * @param $id
	 */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Store a new news.
     *
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(array $attributes)
    {
        $this->beginTransaction();

        $newNews = $this->repository->create($attributes);

        $this->commit();

        return $newNews;
    }

    /*
	 * Update data from news.
	 *
	 * @param $request
	 * @param $id
	 */
    public function update(NewsRequest $request, $id)
    {
        $news = $this->repository->find($id)->update($request->all());

        return $news;
    }

    /*
	 * Delete a news.
	 *
	 * @param $id
	 */
    public function destroy($id)
    {
        $this->repository->find($id)->delete();
    }
}