<?php

namespace Sgpc\Business;

use Sgpc\Http\Requests\ProjectRequest;
use Sgpc\Repositories\ProjectRepository;
use Sgpc\Models\User;
use Sgpc\Models\Profile;
use Sgpc\Models\ParticipantProject;
use Sgpc\Models\Institution;

class ProjectBO extends AbstractBO
{
    /**
     * @var ProjectRepository
     */
    private $repository;

    /**
     * UserBO constructor.
     * @param ProjectRepository $repository
     */
    public function __construct(ProjectRepository $repository)
    {
        $this->repository = $repository;
    }

    /*
	* Show all projects
	*/
    public function all()
    {
        return $this->repository->all();
    }

    /*
	 * Show a specific user.
	 *
	 * @param $id
	 */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Salva um novo usuário.
     *
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(array $attributes)
    {
        $this->beginTransaction();

        $newProject = $this->repository->create($attributes);

        $this->commit();

        return $newProject;
    }

    /*
	 * Update data from users.
	 *
	 * @param $request
	 * @param $id
	 */
    public function update(ProjectRequest $request, $id)
    {
        $project = $this->repository->find($id)->update($request->all());

        return $project;
    }

    /*
	 * Delete a project.
	 *
	 * @param $id
	 */
    public function destroy($id)
    {
        $this->repository->find($id)->delete();
    }

    /*
    *
    */
    public function addUserToProject($attributes)
    {
        $project = $this->repository->find($attributes->project_id);
        $profile = Profile::find($attributes->profile_id);
        $user = User::find($attributes->user_id);

        $participantProject = new ParticipantProject();
        $participantProject->user()->associate($user);
        $participantProject->project()->associate($project);
        $participantProject->profile()->associate($profile);
        $participantProject->save();
    }

    /*
    *
    */
    public function addInstitutionToProject($attributes)
    {
        $project = $this->repository->find($attributes->project_id);
        $project->institutions()->attach($attributes->institution_id);
    }
}