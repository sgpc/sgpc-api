<?php

namespace Sgpc\Business;

use Sgpc\Http\Requests\UsefulLinkRequest;
use Sgpc\Repositories\UsefulLinkRepository;

class UsefulLinkBO extends AbstractBO
{
    /**
     * @var UsefulLinkRepository
     */
    private $repository;

    /**
     * UsefulLinkBO constructor.
     * @param UsefulLinkRepository $repository
     */
    public function __construct(UsefulLinkRepository $repository)
    {
        $this->repository = $repository;
    }

    /*
	* Show all UsefulLinks
	*/
    public function all()
    {
        return $this->repository->all();
    }

    /*
	 * Show a specific user.
	 *
	 * @param $id
	 */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Salva um novo usuário.
     *
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(array $attributes)
    {
        $this->beginTransaction();

        $newUsefulLink = $this->repository->create($attributes);

        $this->commit();

        return $newUsefulLink;
    }

    /*
	 * Update data from users.
	 *
	 * @param $request
	 * @param $id
	 */
    public function update(UsefulLinkRequest $request, $id)
    {
        $UsefulLink = $this->repository->find($id)->update($request->all());

        return $UsefulLink;
    }

    /*
	 * Delete a UsefulLink.
	 *
	 * @param $id
	 */
    public function destroy($id)
    {
        $this->repository->find($id)->delete();
    }
}