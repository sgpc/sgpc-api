<?php

namespace Sgpc\Business;

use Illuminate\Http\Response;
use Sgpc\Repositories\UserRepository;
use Sgpc\Util\Messages;

class UserBO extends AbstractBO
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * UserBO constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * List all users
     *
     * @return mixed
     */
    public function all()
    {
        return $this->repository->with(['profile'])->all();
    }

    /**
     * Find specific user
     *
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $user = (object) $this->repository->findWhere(['id' => $id])->toArray()[0];

        return $user;
    }

    /**
     * Save new user.
     *
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(array $attributes)
    {
        try {
            $this->beginTransaction();

            $attributes['password'] = bcrypt($attributes['password']);

            $newUser = $this->repository->create($attributes);

            $this->commit();

            return response()->json([
                'message' => Messages::SUCCESS,
                'data' => $newUser
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            $this->rollback();
            return response()->json([
                'data' => [
                    'message' => Messages::NOT_CREATED,
                    'errors' => $e->getMessage()
                ]
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update data from users.
     *
     * @param array $attributes
     * @param $id
     * @return mixed
     * @internal param UserRequest $request
     */
    public function update(array $attributes, $id)
    {
        try{
            $this->beginTransaction();

            $user = $this->repository->update($attributes, $id);

            $this->commit();

            return response()->json([
                'message' => Messages::UPDATE,
                'data' => $user
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            $this->rollback();
            return response()->json([
                'data' => [
                    'message' => Messages::NOT_UPDATED,
                    'errors' => $e->getMessage()
                ]
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Delete a user
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->beginTransaction();

            $this->repository->delete($id);

            $this->commit();

            return response()->json([
                'data' => [
                    'message' => Messages::DELETE
                ]
            ], Response::HTTP_NO_CONTENT);

        } catch (\Exception $e) {
            $this->rollback();
            return response()->json([
                'data' => [
                    'message' => Messages::NOT_DELETED,
                    'errors' => $e->getMessage()
                ]
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}