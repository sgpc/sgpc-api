<?php

namespace Sgpc\Business;

use Sgpc\Http\Requests\ProductionRequest;
use Sgpc\Repositories\ProductionRepository;
use Sgpc\Models\Profile;
use Sgpc\Models\User;
use Sgpc\Models\ParticipantProduction;

class ProductionBO extends AbstractBO
{
    /**
     * @var ProductionRepository
     */
    private $repository;

    /**
     * UserBO constructor.
     * @param ProductionRepository $repository
     */
    public function __construct(ProductionRepository $repository)
    {
        $this->repository = $repository;
    }

    /*
	* Show all Productions
	*/
    public function all()
    {
        return $this->repository->all();
    }

    /*
	 * Show a specific user.
	 *
	 * @param $id
	 */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Salva um novo usuário.
     *
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(array $attributes)
    {
        $this->beginTransaction();

        $newProduction = $this->repository->create($attributes);

        $this->commit();

        return $newProduction;
    }

    /*
	 * Update data from users.
	 *
	 * @param $request
	 * @param $id
	 */
    public function update(ProductionRequest $request, $id)
    {
        $production = $this->repository->find($id)->update($request->all());

        return $production;
    }

    /*
	 * Delete a Production.
	 *
	 * @param $id
	 */
    public function destroy($id)
    {
        $this->repository->find($id)->delete();
    }

    /*
    *
    */
    public function addUserToProduction($attributes)
    {
        $production = $this->repository->find($attributes->production_id);
        $profile = Profile::find($attributes->profile_id);
        $user = User::find($attributes->user_id);

        $participantProduction = new ParticipantProduction();
        $participantProduction->user()->associate($user);
        $participantProduction->production()->associate($production);
        $participantProduction->profile()->associate($profile);
        $participantProduction->save();
    }
}