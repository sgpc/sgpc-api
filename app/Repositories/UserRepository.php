<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace Sgpc\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
