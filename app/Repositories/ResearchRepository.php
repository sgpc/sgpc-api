<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ResearchRepository
 * @package namespace Sgpc\Repositories;
 */
interface ResearchRepository extends RepositoryInterface
{
    //
}
