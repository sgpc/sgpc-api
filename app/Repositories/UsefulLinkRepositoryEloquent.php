<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Sgpc\Repositories\UsefulLinkRepository;
use Sgpc\Models\UsefulLink;

/**
 * Class UsefulLinkRepositoryEloquent
 * @package namespace Sgpc\Repositories;
 */
class UsefulLinkRepositoryEloquent extends BaseRepository implements UsefulLinkRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UsefulLink::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
