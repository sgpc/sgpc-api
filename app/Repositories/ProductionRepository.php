<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductionRepository
 * @package namespace Sgpc\Repositories;
 */
interface ProductionRepository extends RepositoryInterface
{
    //
}
