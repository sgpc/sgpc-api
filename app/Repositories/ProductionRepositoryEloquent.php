<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Sgpc\Repositories\ProductionRepository;
use Sgpc\Models\Production;
use Sgpc\Validators\ProductionValidator;

/**
 * Class ProductionRepositoryEloquent
 * @package namespace Sgpc\Repositories;
 */
class ProductionRepositoryEloquent extends BaseRepository implements ProductionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Production::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
