<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UsefulLinkRepository
 * @package namespace Sgpc\Repositories;
 */
interface UsefulLinkRepository extends RepositoryInterface
{
    //
}
