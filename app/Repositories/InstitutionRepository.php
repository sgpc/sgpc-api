<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InstitutionRepository
 * @package namespace Sgpc\Repositories;
 */
interface InstitutionRepository extends RepositoryInterface
{
    //
}
