<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsRepository
 * @package namespace Sgpc\Repositories;
 */
interface NewsRepository extends RepositoryInterface
{
    //
}
