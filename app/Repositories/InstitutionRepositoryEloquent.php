<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Sgpc\Repositories\InstitutionRepository;
use Sgpc\Models\Institution;
use Sgpc\Validators\InstitutionValidator;

/**
 * Class InstitutionRepositoryEloquent
 * @package namespace Sgpc\Repositories;
 */
class InstitutionRepositoryEloquent extends BaseRepository implements InstitutionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Institution::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
