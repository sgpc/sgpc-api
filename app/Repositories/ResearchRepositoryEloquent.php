<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Sgpc\Repositories\ResearchRepository;
use Sgpc\Models\Research;
use Sgpc\Validators\ResearchValidator;

/**
 * Class ResearchRepositoryEloquent
 * @package namespace Sgpc\Repositories;
 */
class ResearchRepositoryEloquent extends BaseRepository implements ResearchRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Research::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
