<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectRepository
 * @package namespace Sgpc\Repositories;
 */
interface ProjectRepository extends RepositoryInterface
{
    //
}
