<?php

namespace Sgpc\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Sgpc\Repositories\UserRepository;
use Sgpc\Models\User;
use Sgpc\Validators\UserValidator;

/**
 * Class UserRepositoryEloquent
 * @package namespace Sgpc\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
