<?php

namespace Sgpc\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sgpc\Http\Controllers\Controller;
use Sgpc\Models\Research;
use Sgpc\Business\ResearchBO;
use Sgpc\Util\Messages;
use Sgpc\Http\Requests\ResearchRequest;
use Illuminate\Http\Response;

class ResearchesController extends Controller
{
    /**
     * @var researchBO.
     */
    private $researchBO;

    /**
     * UsefulLinksController constructor.
     * @param usefulLink $researchBO
     */
    public function __construct(researchBO $researchBO)
    {
        $this->researchBO = $researchBO;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(['data' => $this->researchBO->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->all();
        $research = $this->researchBO->store($attributes);

        return response()->json(['data' => $research, 'message' => Messages::SUCCESS], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Sgpc\Models\Research  $research
     * @return \Illuminate\Http\Response
     */
    public function show(Research $research)
    {
        if (empty($research)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        return response(['data' => $research]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Sgpc\Models\Research  $research
     * @return \Illuminate\Http\Response
     */
    public function update(ResearchRequest $request, Research $research)
    {
        if (empty($research)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->researchBO->update($request, $research->id);

        return response(['data' => ['message' => Messages::UPDATE]]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Sgpc\Models\Research  $research
     * @return \Illuminate\Http\Response
     */
    public function destroy(Research $research)
    {
        if (empty($research)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->researchBO->destroy($research->id);

        return response(['data' => ['message' => Messages::DELETE]]);
    }

    public function addUserToResearch(Request $request)
    {
        $userResearch = $this->researchBO->addUserToResearch((object)$request->all());

        return response()->json(['message' => Messages::SUCCESS], Response::HTTP_CREATED);
    }

    public function addInstitutionToResearch(Request $request)
    {
        $userResearch = $this->researchBO->addInstitutionToResearch((object)$request->all());

        return response()->json(['message' => Messages::SUCCESS], Response::HTTP_CREATED);
    }

    public function countResearches()
    {
        return response(['data' => Research::count()]);
    }
}
