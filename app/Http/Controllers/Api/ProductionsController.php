<?php

namespace Sgpc\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sgpc\Http\Controllers\Controller;
use Sgpc\Models\Production;
use Sgpc\Business\ProductionBO;
use Sgpc\Util\Messages;
use Sgpc\Http\Requests\ProductionRequest;
use Illuminate\Http\Response;

class ProductionsController extends Controller
{
    /**
     * @var productionBO.
     */
    private $productionBO;

    /**
     * UsefulLinksController constructor.
     * @param usefulLink $productionBO
     */
    public function __construct(productionBO $productionBO)
    {
        $this->productionBO = $productionBO;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(['data' => $this->productionBO->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->all();
        $production = $this->productionBO->store($attributes);

        return response()->json(['data' => $production, 'message' => Messages::SUCCESS], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Sgpc\Models\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function show(Production $production)
    {
        if (empty($production)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        return response(['data' => $production]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Sgpc\Models\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function update(ProductionRequest $request, Production $production)
    {
        if (empty($production)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->productionBO->update($request, $production->id);

        return response(['data' => ['message' => Messages::UPDATE]]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Sgpc\Models\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function destroy(Production $production)
    {
        if (empty($production)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->productionBO->destroy($production->id);

        return response(['data' => ['message' => Messages::DELETE]]);
    }

    public function addUserToProduction(Request $request)
    {
        $userProduction = $this->productionBO->addUserToProduction((object)$request->all());

        return response()->json(['message' => Messages::SUCCESS], Response::HTTP_CREATED);
    }
}
