<?php

namespace Sgpc\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sgpc\Http\Controllers\Controller;
use Sgpc\Business\UserBO;
use Sgpc\Util\Messages;
use Sgpc\Http\Requests\UserRequest;
use Illuminate\Http\Response;
use Sgpc\Models\User;

class UsersController extends Controller
{
    /**
     * @var UserBO.
     */
    private $userBO;

    /**
     * UsersController constructor.
     * @param UserBO $userBO
     */
    public function __construct(UserBO $userBO)
    {
        $this->userBO = $userBO;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userBO->all();

        if ($users->isEmpty()) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        return response(['data' => $users], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserRequest $request)
    {
        $attributes = $request->all();
        $user =  $this->userBO->store($attributes);

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userBO->find($id);

        if (empty($user)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        return response(['data' => $user], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request|UserRequest $request
     * @param  $id
     * @return Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = $this->userBO->find($id);

        if (empty($user)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $attributes = $request->all();
        $user = $this->userBO->update($attributes, $user->id);

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $user = $this->userBO->find($id);

        if (empty($user)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $user = $this->userBO->destroy($user->id);

        return $user;
    }

    public function countUsers()
    {
        return response(['data' => User::count()]);
    }
}
