<?php

namespace Sgpc\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sgpc\Http\Controllers\Controller;
use Sgpc\Models\UsefulLink;
use Sgpc\Business\UsefulLinkBO;
use Sgpc\Util\Messages;
use Sgpc\Http\Requests\UsefulLinkRequest;
use Illuminate\Http\Response;

class UsefulLinksController extends Controller
{
    /**
     * @var UsefulLinkBO.
     */
    private $usefulLinkBO;

    /**
     * UsefulLinksController constructor.
     * @param usefulLink $usefulLinkBO
     */
    public function __construct(UsefulLinkBO $usefulLinkBO)
    {
        $this->usefulLinkBO = $usefulLinkBO;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(['data' => $this->usefulLinkBO->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsefulLinkRequest $request)
    {
        $attributes = $request->all();
        $usefulLink = $this->usefulLinkBO->store($attributes);

        return response()->json(['data' => $usefulLink, 'message' => Messages::SUCCESS], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usefulLink = $this->usefulLinkBO->show($id);

        if (empty($usefulLink)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        return response(['data' => $usefulLink]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsefulLinkRequest $request, $id)
    {
        $usefulLink = $this->usefulLinkBO->show($id);

        if (empty($usefulLink)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->usefulLinkBO->update($request, $id);

        return response(['data' => ['message' => Messages::UPDATE]]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usefulLink = $this->usefulLinkBO->show($id);

        if (empty($usefulLink)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->usefulLinkBO->destroy($id);

        return response(['data' => ['message' => Messages::DELETE]]);
    }
}
