<?php

namespace Sgpc\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sgpc\Http\Controllers\Controller;
use Sgpc\Models\Institution;
use Sgpc\Business\InstitutionBO;
use Sgpc\Util\Messages;
use Sgpc\Http\Requests\InstitutionRequest;
use Illuminate\Http\Response;

class InstitutionsController extends Controller
{
    /**
     * @var institutionBO.
     */
    private $institutionBO;

    /**
     * UsefulLinksController constructor.
     * @param usefulLink $institutionBO
     */
    public function __construct(InstitutionBO $institutionBO)
    {
        $this->institutionBO = $institutionBO;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(['data' => $this->institutionBO->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->all();
        $institution = $this->institutionBO->store($attributes);

        return response()->json(['data' => $institution, 'message' => Messages::SUCCESS], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Sgpc\Models\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function show(Institution $institution)
    {
        if (empty($institution)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        return response(['data' => $institution]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Sgpc\Models\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function update(InstitutionRequest $request, Institution $institution)
    {
        if (empty($institution)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->institutionBO->update($request, $institution->id);

        return response(['data' => ['message' => Messages::UPDATE]]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Sgpc\Models\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function destroy(Institution $institution)
    {
        if (empty($institution)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->institutionBO->destroy($institution->id);

        return response(['data' => ['message' => Messages::DELETE]]);
    }
}
