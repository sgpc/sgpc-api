<?php

namespace Sgpc\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sgpc\Business\NewsBO;
use Sgpc\Http\Controllers\Controller;
use Sgpc\Util\Messages;
use Sgpc\Http\Requests\NewsRequest;
use Illuminate\Http\Response;
use Sgpc\Models\News;

class NewsController extends Controller
{
    /**
     * @var ProjectBO.
     */
    private $newsBO;

    /**
     * NewsController constructor.
     * @param NewsBO $projectBO
     */
    public function __construct(NewsBO $newsBO)
    {
        $this->newsBO = $newsBO;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(['data' => $this->newsBO->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->all();
        $news = $this->newsBO->store($attributes);

        return response()->json(['data' => $news, 'message' => Messages::SUCCESS], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = $this->newsBO->show($id);

        if (empty($news)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        return response(['data' => $news]);//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsRequest $request, $id)
    {
        $news = $this->newsBO->show($id);

        if (empty($news)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->newsBO->update($request, $id);

        return response(['data' => ['message' => Messages::UPDATE]]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = $this->newsBO->show($id);

        if (empty($news)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->newsBO->destroy($id);

        return response(['data' => ['message' => Messages::DELETE]]);
    }

    public function countNews()
    {
        return response(['data' => News::count()]);
    }
}
