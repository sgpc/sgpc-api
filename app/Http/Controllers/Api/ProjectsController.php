<?php

namespace Sgpc\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sgpc\Http\Controllers\Controller;
use Sgpc\Business\ProjectBO;
use Sgpc\Util\Messages;
use Sgpc\Http\Requests\ProjectRequest;
use Illuminate\Http\Response;
use Sgpc\Models\Project;

class ProjectsController extends Controller
{
    /**
     * @var ProjectBO.
     */
    private $projectBO;

    /**
     * ProjectsController constructor.
     * @param ProjectBO $projectBO
     */
    public function __construct(ProjectBO $projectBO)
    {
        $this->projectBO = $projectBO;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(['data' => $this->projectBO->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->all();
        $project = $this->projectBO->store($attributes);

        return response()->json(['data' => $project, 'message' => Messages::SUCCESS], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = $this->projectBO->show($id);

        if (empty($project)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        return response(['data' => $project]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request|ProjectRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(ProjectRequest $request, $id)
    {
        $project = $this->projectBO->show($id);

        if (empty($project)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->projectBO->update($request, $id);

        return response(['data' => ['message' => Messages::UPDATE]]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = $this->projectBO->show($id);

        if (empty($project)) {
            return response(['data' => ['message' => Messages::NOT_FOUND]], Response::HTTP_NOT_FOUND);
        }

        $this->projectBO->destroy($id);

        return response(['data' => ['message' => Messages::DELETE]]);
    }

    /**
    *
    */
    public function addUserToProject(Request $request)
    {
        $userProject = $this->projectBO->addUserToProject((object)$request->all());
    }

    public function addInstitutionToProject(Request $request)
    {
        $institutionProject = $this->projectBO->addInstitutionToProject((object)$request->all());
    }

    public function countProjects()
    {
        return response(['data' => Project::count()]);
    }
}
