<?php

namespace Sgpc\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sgpc\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Sgpc\Models\Profile;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(['data' => Profile::all()]);
    }
}
