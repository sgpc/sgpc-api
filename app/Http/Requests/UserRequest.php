<?php

namespace Sgpc\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed user
 */
class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array(
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $this->user,
            'password' => 'required',
            'cpf' => 'required',
            'status' => 'required|in:N,S',
            'profile_id' => 'required'
        );
    }

    public function messages()
    {
        $messages =  [
            'name.required' => 'Campo Nome é obrigatório',
            'email.required' => 'Campo Email é obrigatório',
            'email.unique' => 'O Email já está sendo utilizado',
            'password.required' => 'Campo senha é obrigatório',
            'cpf.required' => 'Campo CPF é obrigatório',
            'status.required' => 'Campo Status é obrigatório',
            'profile_id.required' => 'Campo Perfil é obrigatório',
            'profile_id.exists' => 'Este perfil não existe'
        ];

        return $messages;
    }
}
