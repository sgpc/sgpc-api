<?php

namespace Sgpc\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status_posted' => 'required',
            'link' => 'required',
            'text' => 'required',
            'reference' => 'required',
            'comments' => 'required',
            'url_cover' => 'required',
            'status' => 'required', 
            'production_type_id' => 'required'
        ];
    }
}
